package com.eljacko.charityshop.webapi.controller;

import static org.hamcrest.Matchers.equalTo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WebApiController.class)
@AutoConfigureMockMvc
@SuppressWarnings({ "checkstyle:MethodName" })
public class WebApiControllerTest {

    @Autowired
    private MockMvc mockMvc;

    private static final String ENDPOINT = "/v1/web-api/ping";

    @Test
    public final void getPing_correctQuery_Ok() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(ENDPOINT)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("OK")));
    }

}
