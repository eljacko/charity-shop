package com.eljacko.charityshop.webapi.controller.common;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings({ "checkstyle:MethodName", "checkstyle:MagicNumber" })
public class AbstractCommonControllerTestGetPageRequest {
    @InjectMocks
    private TestAbstractCommonController controller;

    @Test
    public final void getPageRequest_OffsetAndLimitNulls_ReturnWithZeros() {
        final Integer offset = null;
        final Integer limit = null;

        PageRequest actual = controller.getPageRequest(offset, limit);
        assertThat(actual, nullValue());
    }

    @Test
    public final void getPageRequest_OffsetIsNull_ReturnOffsetZero() {
        final Integer offset = null;
        final Integer limit = 23;

        PageRequest actual = controller.getPageRequest(offset, limit);
        assertThat(actual.getPageNumber(), is(0));
        assertThat(actual.getPageSize(), is(limit));
    }

    @Test
    public final void getPageRequest_CorrectPageOffsetAndLimit_ReturnDefinedPageNumberAndSize() {
        final Integer offset = 23;
        final Integer limit = 10;

        PageRequest actual = controller.getPageRequest(offset, limit);
        assertThat("Wrong pageNumber", actual.getPageNumber(), is(offset));
        assertThat("Wrong pageSize", actual.getPageSize(), is(limit));
    }

    @Controller
    static class TestAbstractCommonController extends AbstractCommonController {

    }
}
