package com.eljacko.charityshop.webapi.controller;

import com.eljacko.charityshop.datamodel.constant.field.ProductListItemField;
import com.eljacko.charityshop.datamodel.dto.PageResultWrapper;
import com.eljacko.charityshop.datamodel.dto.product.ProductListItem;
import com.eljacko.charityshop.webapi.constant.ProductSortingKeys;
import com.eljacko.charityshop.webapi.service.ProductListService;
import com.eljacko.charityshop.webapi.service.util.DateUtilService;
import lombok.Getter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = { ProductListController.class })
@SuppressWarnings({"checkstyle:MethodName", "checkstyle:MagicNumber"})
public class ProductListControllerGetProductsTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private DateUtilService dateUtilService;
    @MockBean
    private ProductListService productListService;

    @Getter
    private static final String ORIGIN = "http://localhost";
    private static final String ENDPOINT = "/v1/products";

    @Test
    public final void getProducts_LimitMissing_BadRequest() throws Exception {
        when(dateUtilService.getInstantNow()).thenReturn(Instant.parse("2020-10-23T10:12:35Z"));
        mockMvc.perform(get(ENDPOINT)
                .header(org.springframework.http.HttpHeaders.ORIGIN, ORIGIN)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print()).andExpect(status().isBadRequest());
    }

    @Test
    public final void getProducts_WrongOrderBy_BadRequest() throws Exception {
        final int limit = 10;
        final String orderBy = "wrong_order_by";

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>(2);
        params.add("limit", String.valueOf(limit));
        params.add("orderBy", orderBy);

        when(dateUtilService.getInstantNow()).thenReturn(Instant.parse("2020-10-23T10:12:35Z"));
        mockMvc.perform(get(ENDPOINT).params(params)
                .header(org.springframework.http.HttpHeaders.ORIGIN, ORIGIN)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print()).andExpect(status().isBadRequest());
    }

    @Test
    public final void getProducts_WrongField_BadRequest() throws Exception {
        final int limit = 10;
        final String fieldsStr = "test";

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>(2);
        params.add("limit", String.valueOf(limit));
        params.add("fields", fieldsStr);

        when(dateUtilService.getInstantNow()).thenReturn(Instant.parse("2020-10-23T10:12:35Z"));
        mockMvc.perform(get(ENDPOINT)
                .params(params)
                .header(org.springframework.http.HttpHeaders.ORIGIN, ORIGIN)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print()).andExpect(status().isBadRequest());
    }

    @Test
    public final void getProducts_IdNameFields_FoundOne() throws Exception {
        final int limit = 10;
        final String fieldsStr = ProductListItemField.ID + "," + ProductListItemField.NAME;
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>(2);
        params.add("limit", String.valueOf(limit));
        params.add("fields", fieldsStr);

        Set<ProductListItemField> fields = new HashSet<>(3);
        fields.add(ProductListItemField.ID);
        fields.add(ProductListItemField.NAME);
        PageRequest page = PageRequest.of(0, limit);
        PageResultWrapper<ProductListItem> result = new PageResultWrapper<>();
        result.setTotal(1L);
        result.setResults(new ArrayList<>(1));
        ProductListItem expectedResultItem = new ProductListItem(35L, "Unit Test Product");

        result.getResults().add(expectedResultItem);
        when(dateUtilService.getInstantNow()).thenReturn(Instant.parse("2020-10-23T10:12:35Z"));
        when(productListService.getProducts(page, null, fields)).thenReturn(result);

        mockMvc.perform(get(ENDPOINT).params(params)
                .header(org.springframework.http.HttpHeaders.ORIGIN, ORIGIN)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print()).andExpect(status().isOk())
                .andExpect(jsonPath("$.totalCount", is(result.getTotal().intValue())))
                .andExpect(jsonPath("$.products[0].id",
                        is(expectedResultItem.getId().intValue())))
                .andExpect(jsonPath("$.products[0].name", is(expectedResultItem.getName())))
                .andExpect(jsonPath("$.products[0].category").doesNotExist());
    }

    @Test
    public final void getProducts_RequiredParams_FoundOne() throws Exception {
        final int limit = 2;
        Set<ProductListItemField> fields = new HashSet<>(0);

        PageRequest page = PageRequest.of(0, limit);
        PageResultWrapper<ProductListItem> result = new PageResultWrapper<>();
        result.setTotal(1L);
        result.setResults(new ArrayList<>(1));
        ProductListItem expectedResultItem = new ProductListItem(56L, "Unit Test Product");

        result.getResults().add(expectedResultItem);
        when(dateUtilService.getInstantNow()).thenReturn(Instant.parse("2020-10-23T10:12:35Z"));
        when(productListService.getProducts(page, null, fields)).thenReturn(result);
        mockMvc.perform(get(ENDPOINT)
                        .param("limit", String.valueOf(limit))
                        .header(org.springframework.http.HttpHeaders.ORIGIN, ORIGIN)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print()).andExpect(status().isOk())
                .andExpect(jsonPath("$.totalCount", is(result.getTotal().intValue())))
                .andExpect(jsonPath("$.products[0].id", is(expectedResultItem.getId().intValue())))
                .andExpect(jsonPath("$.products[0].name", is(expectedResultItem.getName())));
    }

    @Test
    public final void getProducts_AllParams_FoundOne() throws Exception {
        final String orderBy = ProductSortingKeys.NAME + ":asc";
        Sort sort = getSort(orderBy, ProductSortingKeys.values());
        final int limit = 2;
        final int offset = 0;
        Set<ProductListItemField> fields = getAllFields();

        PageRequest page = PageRequest.of(offset, limit);
        PageResultWrapper<ProductListItem> result = new PageResultWrapper<>();
        result.setTotal(1L);
        result.setResults(new ArrayList<>(1));
        ProductListItem expectedResultItem = new ProductListItem(56L, "Unit Test Product");

        result.getResults().add(expectedResultItem);
        when(dateUtilService.getInstantNow()).thenReturn(Instant.parse("2020-10-23T10:12:35Z"));
        when(productListService.getProducts(page, sort, fields)).thenReturn(result);

        final String fieldsStr = fields.stream().map(f -> f.toString())
                .collect(Collectors.joining(","));
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>(2);
        params.add("limit", String.valueOf(limit));
        params.add("fields", fieldsStr);
        params.add("orderBy", orderBy);
        params.add("offset", String.valueOf(offset));

        mockMvc.perform(get(ENDPOINT)
                .params(params)
                .header(org.springframework.http.HttpHeaders.ORIGIN, ORIGIN)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print()).andExpect(status().isOk())
                .andExpect(jsonPath("$.totalCount", is(result.getTotal().intValue())))
                .andExpect(jsonPath("$.products[0].id", is(expectedResultItem.getId().intValue())))
                .andExpect(jsonPath("$.products[0].name", is(expectedResultItem.getName())));
    }

    private Sort getSort(final String sortingData, final Set<String> allowedValues) {
        Sort sort = null;
        if (!StringUtils.isEmpty(sortingData)) {
            List<Sort.Order> sortOrders = Arrays.asList(sortingData.split(",")).stream()
                    .map(String::trim).map(str -> str.split(":"))
                    .map(arr -> getSortObj(arr, allowedValues)).collect(Collectors.toList());

            sort = Sort.by(sortOrders);
        }
        return sort;
    }

    private Sort.Order getSortObj(final String[] arr, final Set<String> allowedValues) {
        Sort.Order sort = null;
        if (arr.length > 0) {
            String orderBy = arr[0].trim().toLowerCase();
            if (!allowedValues.contains(orderBy)) {
                throw new IllegalArgumentException("Unknown order property");
            }
            Sort.Direction orderDirection = Sort.Direction.ASC;
            if (arr.length == 2) {
                String orderDirectionStr = arr[1].trim().toLowerCase();
                orderDirection = Sort.Direction.fromString(orderDirectionStr);
            }
            sort = new Sort.Order(orderDirection, orderBy);
        }
        return sort;
    }

    private Set<ProductListItemField> getAllFields() {
        return Stream.of(ProductListItemField.values()).collect(Collectors.toSet());
    }
}
