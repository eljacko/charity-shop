package com.eljacko.charityshop.webapi.util;

import com.eljacko.charityshop.webapi.dto.transaction.TransactionData;
import com.eljacko.charityshop.webapi.dto.transaction.TransactionDataSaveResult;

import java.util.function.Consumer;

@SuppressWarnings({ "checkstyle:MagicNumber" })
public final class MockedObjectBuilder {

    public static TransactionData getTransactionData(final Long id,
            final Consumer<TransactionData> dataSetter) {
        TransactionData mock = new TransactionData();
        mock.setId(id);
        if (dataSetter != null) {
            dataSetter.accept(mock);
        }
        return mock;
    }

    public static TransactionDataSaveResult getTransactionDataSaveResult(final Long transactionId,
            final Consumer<TransactionDataSaveResult> dataSetter) {
        TransactionDataSaveResult mock = new TransactionDataSaveResult();
        TransactionData transactionData = getTransactionData(6L, null);
        mock.setTransactionData(transactionData);
        mock.setTransactionData(getTransactionData(transactionId, null));
        if (dataSetter != null) {
            dataSetter.accept(mock);
        }
        return mock;
    }

    private MockedObjectBuilder() {
        throw new UnsupportedOperationException(
                "This is a utility class and cannot be instantiated");
    }
}
