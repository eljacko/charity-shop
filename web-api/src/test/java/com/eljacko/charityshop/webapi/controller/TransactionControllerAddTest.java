package com.eljacko.charityshop.webapi.controller;

import com.eljacko.charityshop.webapi.constant.ErrorCodes;
import com.eljacko.charityshop.webapi.dto.transaction.TransactionData;
import com.eljacko.charityshop.webapi.dto.transaction.TransactionDataSaveResult;
import com.eljacko.charityshop.webapi.service.JSONService;
import com.eljacko.charityshop.webapi.service.TransactionService;
import com.eljacko.charityshop.webapi.service.util.DateUtilService;
import com.google.gson.Gson;
import lombok.Getter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import java.time.Instant;
import java.util.ArrayList;
import java.util.function.Consumer;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.Mockito.when;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = { TransactionController.class })
@AutoConfigureWebMvc
@SuppressWarnings({"checkstyle:MethodName", "checkstyle:MagicNumber"})
public class TransactionControllerAddTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private TransactionService transactionService;
    @MockBean
    private DateUtilService dateUtilService;
    @MockBean
    private JSONService jsonService;

    @Getter
    private static final String ORIGIN = "http://localhost";
    private static final String ENDPOINT = "/v1/transactions";

    @Test
    public final void addTransaction_NoContent_BadRequest() throws Exception {
        when(dateUtilService.getInstantNow()).thenReturn(Instant.parse("2020-10-23T10:12:35Z"));
        mockMvc.perform(post(ENDPOINT)
                        .header(org.springframework.http.HttpHeaders.ORIGIN, ORIGIN)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print()).andExpect(status().isBadRequest());
    }

    @Test
    public final void addTransaction_NoTransactionDetails_BadRequest() throws Exception {
        TransactionData transactionData = getTransactionData(null);
        verifyInvalidData(transactionData, "transactionDetails");
    }

    @Test
    public final void addTransaction_ReceivedPaymentNotNumber_BadRequest() throws Exception {
        TransactionData transactionData = getTransactionData(td -> {
            td.setTransactionDetails(new ArrayList<>());
            td.setReceivedPayment("a");
        });
        verifyInvalidData(transactionData, "receivedPayment");
    }

    @Test
    public final void addTransaction_ReceivedPaymentWithCancel_BadRequest() throws Exception {
        TransactionData transactionData = getTransactionData(td -> {
            td.setTransactionDetails(new ArrayList<>());
            td.setReceivedPayment("10");
            td.setCancelTransaction(true);
        });
        verifyInvalidData(transactionData, "receivedPayment");
    }

    private void verifyInvalidData(final TransactionData transactionData, final String fieldName)
            throws Exception {
        Mockito.when(jsonService.getGson()).thenReturn(new Gson());
        when(dateUtilService.getInstantNow()).thenReturn(Instant.parse("2020-10-23T10:12:35Z"));
        mockMvc.perform(post(ENDPOINT)
                        .header(org.springframework.http.HttpHeaders.ORIGIN, ORIGIN)
                        .content(jsonService.toJson(transactionData))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print()).andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.code", is(ErrorCodes.INVALID_DATA)))
                .andExpect(jsonPath("$.extraData[0].field", is(fieldName)));
    }

    @Test
    public final void addTransaction_Saved() throws Exception {
        TransactionData transactionData = getTransactionData(td -> {
            td.setTransactionDetails(new ArrayList<>());
        });
        TransactionData transactionDataResult = getTransactionData(td -> {
            td.setTransactionDetails(new ArrayList<>());
            td.setId(5L);
        });
        TransactionDataSaveResult result = new TransactionDataSaveResult();
        result.setTransactionData(transactionDataResult);

        Mockito.when(jsonService.getGson()).thenReturn(new Gson());
        Mockito.when(transactionService.save(transactionData)).thenReturn(result);
        mockMvc.perform(post(ENDPOINT)
                .header(org.springframework.http.HttpHeaders.ORIGIN, ORIGIN)
                .content(jsonService.toJson(transactionData))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print()).andExpect(status().isCreated())
                .andExpect(header().string(HttpHeaders.LOCATION, notNullValue()))
                .andExpect(jsonPath("$.transactionData.id",
                        is(result.getTransactionData().getId().intValue())));
    }

    private TransactionData getTransactionData(final Consumer<TransactionData> dataSetter) {
        TransactionData transactionData = new TransactionData();
        if (dataSetter != null) {
            dataSetter.accept(transactionData);
        }
        return transactionData;
    }
}
