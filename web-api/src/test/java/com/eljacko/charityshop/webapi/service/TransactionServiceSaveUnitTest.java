package com.eljacko.charityshop.webapi.service;

import com.eljacko.charityshop.datamodel.entity.product.Product;
import com.eljacko.charityshop.datamodel.entity.transaction.Transaction;
import com.eljacko.charityshop.datamodel.entity.transaction.TransactionDetails;
import com.eljacko.charityshop.datamodel.repository.ProductRepository;
import com.eljacko.charityshop.datamodel.repository.TransactionDetailsRepository;
import com.eljacko.charityshop.datamodel.repository.TransactionRepository;
import com.eljacko.charityshop.webapi.dto.transaction.TransactionData;
import com.eljacko.charityshop.webapi.dto.transaction.TransactionDataSaveResult;
import com.eljacko.charityshop.webapi.exception.NotFoundException;
import com.eljacko.charityshop.webapi.service.util.DateUtilService;
import com.eljacko.charityshop.webapi.util.MockedObjectBuilder;
import com.eljacko.charityshop.webapi.util.UnitTestHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;

@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings({ "checkstyle:MethodName", "checkstyle:magicnumber",
        "checkstyle:VisibilityModifier" })
public class TransactionServiceSaveUnitTest {
    @InjectMocks
    private TransactionServiceImpl service;
    @Mock
    TransactionRepository transactionRepository;
    @Mock
    ProductRepository productRepository;
    @Mock
    TransactionDetailsRepository detailsRepository;
    @Mock
    private DateUtilService dateUtilService;

    @Test
    public final void save_DataIsNull_ThrowIllegalArgumentEx() {
        final TransactionData dto = null;
        Throwable thrown = catchThrowable(() -> {
            service.save(dto);
        });

        assertThat(thrown).isInstanceOf(IllegalArgumentException.class);
        verifyNoInteractions(transactionRepository);
    }

    @Test
    public final void save_UpdateNotExistingTransaction_ThrowNotFoundEx() {
        final TransactionData dto = MockedObjectBuilder.getTransactionData(12L, null);
        Mockito.when(transactionRepository.findById(dto.getId())).thenReturn(Optional.empty());
        Throwable thrown = catchThrowable(() -> {
            service.save(dto);
        });

        assertThat(thrown).isInstanceOf(NotFoundException.class);
        verify(transactionRepository,
                Mockito.never()).save(ArgumentMatchers.any(Transaction.class));
    }

    @Test
    public final void save_SaveNewTransactionNoDetails_SavedTransactionData() {
        TransactionData data = new TransactionData();
        data.setTransactionDetails(new ArrayList<>(1));

        TransactionDataSaveResult result =
                MockedObjectBuilder.getTransactionDataSaveResult(12L, null);

        Timestamp lastAction = UnitTestHelper.currentTimeAsTimestamp();

        Mockito.when(dateUtilService.getCurrentTimeAsTimestamp()).thenReturn(lastAction);
        Mockito.doAnswer(invocation -> {
            Transaction transactionDb = invocation.getArgument(0);
            assertThat(transactionDb.getCancelled(), is(nullValue()));
            assertThat(transactionDb.getPaymentReceived(), is(nullValue()));
            assertThat(transactionDb.getLastAction(), is(lastAction));
            transactionDb.setId(12L);
            return transactionDb;
        }).when(transactionRepository).save(ArgumentMatchers.any(Transaction.class));

        TransactionDataSaveResult savedData = service.save(data);

        verify(transactionRepository,
                Mockito.atLeastOnce()).save(ArgumentMatchers.any(Transaction.class));
        verify(productRepository, Mockito.never()).save(ArgumentMatchers.any(Product.class));
        verify(detailsRepository,
                Mockito.never()).save(ArgumentMatchers.any(TransactionDetails.class));
        assertThat(savedData.getErrorDetails().isEmpty(), is(true));
        assertThat(savedData.getTransactionData().getId(),
                is(result.getTransactionData().getId()));
        assertThat(savedData.isTransactionError(), is(false));
    }

}
