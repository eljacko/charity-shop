package com.eljacko.charityshop.webapi.controller;

import com.eljacko.charityshop.datamodel.constant.field.ProductListItemField;
import com.eljacko.charityshop.datamodel.dto.PageResultWrapper;
import com.eljacko.charityshop.datamodel.dto.product.ProductListItem;
import com.eljacko.charityshop.webapi.constant.ProductSortingKeys;
import com.eljacko.charityshop.webapi.constant.WebApiPaths;
import com.eljacko.charityshop.webapi.dto.product.ProductListResponse;
import com.eljacko.charityshop.webapi.service.ProductListService;
import com.eljacko.charityshop.webapi.service.util.DateUtilService;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/v1")
@SuppressWarnings("checkstyle:designforextension")
public class ProductListController extends AbstractController {

    private final ProductListService productListService;
    private final DateUtilService dateUtilService;

    @GetMapping(WebApiPaths.PRODUCTS)
    @SuppressWarnings({ "checkstyle:FinalParameters" })
    public HttpEntity<?> getProductsForShop(
            @RequestParam(value = "fields", required = false) final String fieldsStr,
            @RequestParam(value = "orderBy", required = false) String sortStr,
            @RequestParam(value = "offset", required = false) final Integer offset,
            @RequestParam(value = "limit", required = true) final Integer limit
    ) {
        log.debug("Request to get products");
        Set<ProductListItemField> fields = new HashSet<>();
        if (!StringUtils.isEmpty(fieldsStr)) {
            fields = Arrays.asList(fieldsStr.split(",")).stream()
                    .map(f -> ProductListItemField.fromString(f)).collect(Collectors.toSet());
        }
        org.springframework.data.domain.Sort sort = getSort(sortStr, ProductSortingKeys.values());
        PageRequest page = PageRequest.of(Optional.ofNullable(offset).orElse(0), limit);
        return getProductListData(page, sort, fields);
    }

    private HttpEntity<?> getProductListData(final PageRequest page,
                                             final Sort sort,
                                             final Set<ProductListItemField> fields) {
        PageResultWrapper<ProductListItem> result = productListService
                .getProducts(page, sort, fields);
        ProductListResponse response = new ProductListResponse();
        response.setTotalCount(result.getTotal());
        response.setProducts(result.getResults());
        response.setTimestampAsStr(dateUtilService.getInstantNow());
        log.debug("Found products data {}", response.toStringMainData());
        MappingJacksonValue mappingJacksonValue = new MappingJacksonValue(response);
        FilterProvider filters;
        if (!CollectionUtils.isEmpty(fields)) {
            Set<String> fieldsStrValues = fields.stream().map(f -> f.getText())
                    .collect(Collectors.toSet());
            filters = new SimpleFilterProvider().addFilter(
                    "com.eljacko.charityshop.datamodel.dto.product.ProductListItem",
                    SimpleBeanPropertyFilter.filterOutAllExcept(fieldsStrValues));
        } else {
            filters = new SimpleFilterProvider().addFilter(
                    "com.eljacko.charityshop.datamodel.dto.product.ProductListItem",
                    SimpleBeanPropertyFilter.serializeAll());
        }
        mappingJacksonValue.setFilters(filters);
        return new ResponseEntity<ProductListResponse>(response, HttpStatus.OK);
    }
}
