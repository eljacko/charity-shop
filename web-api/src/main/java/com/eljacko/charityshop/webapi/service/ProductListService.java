package com.eljacko.charityshop.webapi.service;

import com.eljacko.charityshop.datamodel.constant.field.ProductListItemField;
import com.eljacko.charityshop.datamodel.dto.PageResultWrapper;
import com.eljacko.charityshop.datamodel.dto.product.ProductListItem;
import org.springframework.data.domain.PageRequest;

import java.util.Set;

@SuppressWarnings({ "checkstyle:DesignForExtension" })
public interface ProductListService {

    PageResultWrapper<ProductListItem> getProducts(PageRequest pageRequest,
                                                   org.springframework.data.domain.Sort sort,
                                                   Set<ProductListItemField> fields);


}
