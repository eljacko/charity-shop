package com.eljacko.charityshop.webapi.exception;

@SuppressWarnings({ "checkstyle:FinalParameters" })
public class InsufficientPaymentException extends RuntimeException {
    private static final long serialVersionUID = 4076119456871982779L;

    public InsufficientPaymentException() {
        super();
    }

    public InsufficientPaymentException(final String message) {
        super(message);
    }

    public InsufficientPaymentException(final String message, Throwable cause) {
        super(message, cause);
    }

    public InsufficientPaymentException(Throwable cause) {
        super(cause);
    }
}
