package com.eljacko.charityshop.webapi.dto.transaction;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class TransactionData {
    private Long id;
    private String note;
    private List<TransactionDetailData> transactionDetails;
    private String receivedPayment;
    private BigDecimal change;
    private BigDecimal total;
    private boolean cancelTransaction;

    public TransactionData() {
        super();
    }
}
