package com.eljacko.charityshop.webapi.controller;

import com.eljacko.charityshop.webapi.constant.WebApiPaths;
import com.eljacko.charityshop.webapi.dto.FieldError;
import com.eljacko.charityshop.webapi.dto.transaction.TransactionData;
import com.eljacko.charityshop.webapi.dto.transaction.TransactionDataSaveResult;
import com.eljacko.charityshop.webapi.exception.InvalidParametersException;
import com.eljacko.charityshop.webapi.service.TransactionService;
import com.eljacko.charityshop.webapi.util.StringUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/v1")
@SuppressWarnings({"checkstyle:designforextension"})
public class TransactionController {

    private final TransactionService transactionService;

    @PostMapping(WebApiPaths.TRANSACTIONS)
    public HttpEntity<?> add(@RequestBody @Valid final TransactionData transactionData) {
        log.debug("Request to insert transaction {}", transactionData);

        TransactionDataSaveResult result = save(null, transactionData);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(result.getTransactionData().getId()).toUri();
        log.debug("Transaction data insert is done: transactionId:{}",
                result.getTransactionData().getId());
        return ResponseEntity.created(location).body(result);
    }

    @PutMapping(WebApiPaths.TRANSACTION)
    final HttpEntity<?> update(@PathVariable("transactionId") final Long transactionId,
                               @RequestBody @Valid final TransactionData transactionData) {
        log.debug("Request to update transaction: transactionId:{}, data={}",
                transactionId, transactionId);

        TransactionDataSaveResult result = save(transactionId, transactionData);
        log.debug("Transaction data update is done: transactionId:{}", transactionId);
        return new ResponseEntity<TransactionDataSaveResult>(result, HttpStatus.OK);
    }

    private TransactionDataSaveResult save(final Long transactionId,
                                           final TransactionData transactionData) {
        TransactionDataSaveResult result = null;
        transactionData.setId(transactionId);
        if (isValidTransactionData(transactionData)) {
            result = transactionService.save(transactionData);
        }
        return result;
    }

    private boolean isValidTransactionData(final TransactionData transactionData) {
        List<FieldError> fieldErrors = new ArrayList<>();
        if (transactionData == null) {
            fieldErrors.add(new FieldError("request", "Request can't be empty"));
            throw new InvalidParametersException(fieldErrors);
        }
        if (transactionData.getTransactionDetails() == null) {
            fieldErrors.add(new FieldError("transactionDetails",
                    "Transaction details are required"));
        }
        if (transactionData.getReceivedPayment() != null
                && !StringUtil.isBigDecimalString(transactionData.getReceivedPayment())) {
            fieldErrors.add(new FieldError("receivedPayment",
                    "Not valid payment sum"));
        }
        if (transactionData.getReceivedPayment() != null && transactionData.isCancelTransaction()) {
            fieldErrors.add(new FieldError("receivedPayment",
                    "Payment and cancel used together, unable to finish transaction"));
        }
        if (CollectionUtils.isEmpty(fieldErrors)) {
            return true;
        }
        throw new InvalidParametersException(fieldErrors);
    }
}
