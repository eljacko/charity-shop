package com.eljacko.charityshop.webapi.dto.product;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class ProductImportData {

    private String name;
    private String category;
    private Integer quantity;
    private BigDecimal price;
    @ToString.Exclude
    private byte[] picture;

}


