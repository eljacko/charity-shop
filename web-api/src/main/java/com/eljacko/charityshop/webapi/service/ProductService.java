package com.eljacko.charityshop.webapi.service;

import java.io.File;

public interface ProductService {

    void addProductsFromFile(File products);

}
