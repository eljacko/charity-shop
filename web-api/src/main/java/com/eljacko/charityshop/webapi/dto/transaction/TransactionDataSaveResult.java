package com.eljacko.charityshop.webapi.dto.transaction;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class TransactionDataSaveResult {

    private TransactionData transactionData;
    private boolean transactionError;
    private List<TransactionError> errorDetails;

}
