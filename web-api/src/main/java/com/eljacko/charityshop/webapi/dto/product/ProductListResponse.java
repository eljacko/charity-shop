package com.eljacko.charityshop.webapi.dto.product;

import com.eljacko.charityshop.datamodel.dto.product.ProductListItem;
import com.eljacko.charityshop.webapi.dto.response.CommonTokenResponse;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.util.CollectionUtils;

import java.util.Collection;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class ProductListResponse extends CommonTokenResponse {
    private Long totalCount;
    private Collection<ProductListItem> products;

    public final String toStringMainData() {
        StringBuilder builder = new StringBuilder();
        builder.append("ProductListResponse [totalCount=");
        builder.append(totalCount);
        builder.append(", products size=");
        if (CollectionUtils.isEmpty(products)) {
            builder.append(0);
        } else {
            builder.append(products.size());
        }
        builder.append("]");
        return builder.toString();
    }

}
