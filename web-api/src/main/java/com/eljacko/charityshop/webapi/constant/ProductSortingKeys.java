package com.eljacko.charityshop.webapi.constant;

import java.util.HashSet;
import java.util.Set;

public final class ProductSortingKeys {
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String QUANTITY = "quantity";
    public static final String PRICE = "price";
    public static final String CATEGORY = "category";

    private ProductSortingKeys() {
        throw new UnsupportedOperationException(
                "This is a constants class and cannot be instantiated");
    }

    public static boolean isCorrectSortingKey(final String key) {
        switch (key) {
        case ID:
        case NAME:
        case QUANTITY:
        case PRICE:
        case CATEGORY:
            return true;
        default:
            break;
        }
        return false;
    }

    @SuppressWarnings({ "checkstyle:magicnumber" })
    public static Set<String> values() {
        return new HashSet<String>(5) { {
            add(ID);
            add(NAME);
            add(QUANTITY);
            add(PRICE);
            add(CATEGORY);
        } };
    }
}
