package com.eljacko.charityshop.webapi.service;

import com.eljacko.charityshop.datamodel.entity.product.Product;
import com.eljacko.charityshop.datamodel.entity.transaction.Transaction;
import com.eljacko.charityshop.datamodel.entity.transaction.TransactionDetails;
import com.eljacko.charityshop.datamodel.repository.ProductRepository;
import com.eljacko.charityshop.datamodel.repository.TransactionDetailsRepository;
import com.eljacko.charityshop.datamodel.repository.TransactionRepository;
import com.eljacko.charityshop.webapi.dto.transaction.TransactionData;
import com.eljacko.charityshop.webapi.dto.transaction.TransactionDataSaveResult;
import com.eljacko.charityshop.webapi.dto.transaction.TransactionDetailData;
import com.eljacko.charityshop.webapi.dto.transaction.TransactionError;
import com.eljacko.charityshop.webapi.exception.InsufficientPaymentException;
import com.eljacko.charityshop.webapi.exception.NotFoundException;
import com.eljacko.charityshop.webapi.exception.TransactionClosedException;
import com.eljacko.charityshop.webapi.service.util.DateUtilService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
@SuppressWarnings({ "checkstyle:DesignForExtension" })
public class TransactionServiceImpl implements TransactionService {

    private final TransactionRepository transactionRepository;
    private final TransactionDetailsRepository transactionDetailsRepository;
    private final ProductRepository productRepository;
    private final DateUtilService dateUtilService;

    @Override
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public TransactionDataSaveResult save(final TransactionData transactionData) {
        Assert.notNull(transactionData, "Transaction data must not be null");
        log.debug("Save transaction data id={}", transactionData.getId());
        TransactionDataSaveResult result = new TransactionDataSaveResult();
        result.setErrorDetails(new ArrayList<TransactionError>());

        Transaction transactionDb;
        if (transactionData.getId() == null) {
            transactionDb = new Transaction();
            transactionDb.setTransactionDetails(new HashSet<>());
        } else {
            transactionDb = transactionRepository.findById(transactionData.getId())
                    .orElseThrow(() -> new NotFoundException());
            if (transactionDb.getCancelled() != null) {
                // previous transaction timed out or cancelled,
                // creating new to recover shopping cart as new
                transactionDb = new Transaction();
                transactionDb.setTransactionDetails(new HashSet<>());
            }
            if (transactionDb.getPaymentReceived() != null) {
                // throwing error as payment was already received and avoiding quantity errors
                throw new TransactionClosedException();
            }
        }
        transactionDb.setNote(transactionData.getNote());
        transactionDb.setLastAction(dateUtilService.getCurrentTimeAsTimestamp());
        transactionRepository.save(transactionDb);

        if (transactionData.isCancelTransaction()) {
            transactionDb.setCancelled(dateUtilService.getCurrentTimeAsTimestamp());
        }

        boolean isPaymentReceived = transactionData.getReceivedPayment() != null;
        BigDecimal total = new BigDecimal("0");
        for (TransactionDetailData td : transactionData.getTransactionDetails()) {
            TransactionDetails transactionDetailsDb = null;
            Product productDb = null;
            if (transactionDb.getTransactionDetails() != null) {
                for (TransactionDetails details : transactionDb.getTransactionDetails()) {
                    if (details.getProduct().getId().equals(td.getProductId())) {
                        productDb = details.getProduct();
                        transactionDetailsDb = details;
                    }
                }
            }
            if (transactionDetailsDb == null) {
                transactionDetailsDb = new TransactionDetails();
                productDb = productRepository.findById(td.getProductId())
                        .orElseThrow(() -> new NotFoundException());
                transactionDetailsDb.setProduct(productDb);
                transactionDb.getTransactionDetails().add(transactionDetailsDb);
            }
            Integer reservedAmount = transactionRepository.getReservedAmount(td.getProductId());
            Integer totalAmount = productDb.getQuantity();
            int available = totalAmount - reservedAmount;
            if (transactionDetailsDb.getQuantity() > 0) {
                available = available + transactionDetailsDb.getQuantity();
            }
            if ((available - td.getQuantity()) < 0) {
                transactionDetailsDb.setQuantity(available);
                result.setTransactionError(true);
                result.getErrorDetails().add(new TransactionError("not_enough_items",
                        "Available quantity for " + productDb.getName() + ": " + available));
            } else {
                transactionDetailsDb.setQuantity(td.getQuantity());
            }
            transactionDetailsDb.setPrice(productDb.getPrice());
            transactionDetailsDb.setTransaction(transactionDb);

            total = total.add(productDb.getPrice()
                    .multiply(BigDecimal.valueOf(transactionDetailsDb.getQuantity())));
            transactionDetailsRepository.save(transactionDetailsDb);
            if (isPaymentReceived) {
                productDb.setQuantity(totalAmount - td.getQuantity());
                productRepository.save(productDb);
            }
        }

        TransactionData responseTransactionData = transactionToTransactionData(transactionDb);

        if (isPaymentReceived) {
            BigDecimal receivedPayment = new BigDecimal(transactionData.getReceivedPayment());
            if (receivedPayment.compareTo(total) == -1) {
                throw new InsufficientPaymentException();
            }
            transactionDb.setPaymentReceived(dateUtilService.getCurrentTimeAsTimestamp());
            BigDecimal change = receivedPayment.subtract(total);
            responseTransactionData.setChange(change);
        }
        responseTransactionData.setTotal(total);
        result.setTransactionData(responseTransactionData);
        return result;
    }

    @Override
    @Transactional
    public void processAbandonedCarts() {
        transactionRepository.cancelAbandoned(dateUtilService.getTimestampTenMinutesAgo(),
                dateUtilService.getCurrentTimeAsTimestamp());
    }

    private TransactionData transactionToTransactionData(final Transaction transaction) {
        TransactionData transactionData = new TransactionData();
        transactionData.setId(transaction.getId());
        transactionData.setNote(transaction.getNote());
        if (transaction.getCancelled() != null) {
            transactionData.setCancelTransaction(true);
        }
        List<TransactionDetailData> responseDetails = new ArrayList<>();
        for (TransactionDetails t : transaction.getTransactionDetails()) {
            TransactionDetailData detailData = new TransactionDetailData();
            detailData.setQuantity(t.getQuantity());
            detailData.setProductId(t.getProduct().getId());
            responseDetails.add(detailData);
        }
        transactionData.setTransactionDetails(responseDetails);
        return transactionData;
    }
}
