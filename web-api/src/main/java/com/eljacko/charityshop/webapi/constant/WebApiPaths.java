package com.eljacko.charityshop.webapi.constant;

public final class WebApiPaths {

    public static final String HOME = "/";
    public static final String PRODUCTS = "/products";
    public static final String TRANSACTIONS = "/transactions";
    public static final String TRANSACTION = "/transaction/{transactionId}";


    private WebApiPaths() {
        throw new UnsupportedOperationException(
                "This is a constants class and cannot be instantiated");
    }
}
