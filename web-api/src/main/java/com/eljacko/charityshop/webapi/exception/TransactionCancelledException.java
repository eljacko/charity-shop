package com.eljacko.charityshop.webapi.exception;

@SuppressWarnings({ "checkstyle:FinalParameters" })
public class TransactionCancelledException extends RuntimeException {
    private static final long serialVersionUID = 6076119498371982779L;

    public TransactionCancelledException() {
        super();
    }

    public TransactionCancelledException(final String message) {
        super(message);
    }

    public TransactionCancelledException(final String message, Throwable cause) {
        super(message, cause);
    }

    public TransactionCancelledException(Throwable cause) {
        super(cause);
    }
}
