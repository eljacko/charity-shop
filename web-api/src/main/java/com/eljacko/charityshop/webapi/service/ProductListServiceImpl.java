package com.eljacko.charityshop.webapi.service;

import com.eljacko.charityshop.datamodel.constant.field.ProductListItemField;
import com.eljacko.charityshop.datamodel.dto.PageResultWrapper;
import com.eljacko.charityshop.datamodel.dto.product.ProductListItem;
import com.eljacko.charityshop.datamodel.repository.ProductRepository;
import com.eljacko.charityshop.datamodel.repository.TransactionRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Service
@Slf4j
@RequiredArgsConstructor
@SuppressWarnings({ "checkstyle:DesignForExtension" })
public class ProductListServiceImpl implements ProductListService {

    private final ProductRepository productRepository;
    private final TransactionRepository transactionRepository;

    @Override
    @Transactional(readOnly = true)
    public PageResultWrapper<ProductListItem> getProducts(final PageRequest pageRequest,
                                                          final Sort sort,
                                                          final Set<ProductListItemField> fields) {
        Set<ProductListItemField> verifiedFields = fields;
        if (CollectionUtils.isEmpty(verifiedFields)) {
            verifiedFields = Stream.of(ProductListItemField.values()).collect(Collectors.toSet());
        }
        PageResultWrapper<ProductListItem> result = productRepository
                .getProducts(pageRequest, sort, verifiedFields);
        if (verifiedFields.contains(ProductListItemField.RESERVED_QUANTITY)) {
            for (ProductListItem p : result.getResults()) {
                Integer reservedAmount = transactionRepository.getReservedAmount((Long) p.getId());
                p.setReservedQuantity(reservedAmount);
            }
        }
        if (result != null && result.getResults() != null) {
            log.debug("Found {} products", result.getResults().size());
        }
        return result;
    }
}
