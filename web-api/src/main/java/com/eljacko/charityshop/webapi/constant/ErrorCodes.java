package com.eljacko.charityshop.webapi.constant;

public final class ErrorCodes {

    public static final int INVALID_DATA = 11;
    public static final int NOT_ENOUGH_ITEMS_AVAILABLE = 12;
    public static final int TRANSACTION_CANCELLED = 13;
    public static final int TRANSACTION_CLOSED = 14;

    public static final int UNKNOWN_EXCEPTION = 100;
    public static final int APPLICATION_EXCEPTION = 101;
    public static final int HANDLER_NOT_FOUND = 102;

    private ErrorCodes() {
        throw new UnsupportedOperationException(
                "This is a constants class and cannot be instantiated");
    }
}
