package com.eljacko.charityshop.webapi.util;

import com.eljacko.charityshop.webapi.constant.Loggers;
import io.undertow.server.handlers.accesslog.AccessLogReceiver;

public class WebApiAccessLogReceiver implements AccessLogReceiver {

    @Override
    public final void logMessage(final String message) {
        if (Loggers.ACCESS_LOG_LOGGER.isInfoEnabled()) {
            Loggers.ACCESS_LOG_LOGGER.info(message);
        }
    }
}
