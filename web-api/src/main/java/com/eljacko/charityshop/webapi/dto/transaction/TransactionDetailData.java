package com.eljacko.charityshop.webapi.dto.transaction;

import com.eljacko.charityshop.datamodel.constant.ValidationMessages;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class TransactionDetailData {
    @NotNull(message = ValidationMessages.NOT_NULL)
    private Long productId;
    @NotNull(message = ValidationMessages.NOT_NULL)
    @NotBlank
    private Integer quantity;
}
