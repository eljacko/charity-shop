package com.eljacko.charityshop.webapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableConfigurationProperties
@EnableScheduling
@SuppressWarnings({"checkstyle:designforextension", "checkstyle:HideUtilityClassConstructor"})
public class CharityShopApplication {


    public CharityShopApplication() {
        super();
    }

    public static void main(final String[] args) {
        SpringApplication.run(CharityShopApplication.class, args);
     }
}
