package com.eljacko.charityshop.webapi.service;

import com.eljacko.charityshop.webapi.dto.product.ProductImportData;
import com.eljacko.charityshop.datamodel.entity.product.Category;
import com.eljacko.charityshop.datamodel.entity.product.Product;
import com.eljacko.charityshop.datamodel.repository.CategoryRepository;
import com.eljacko.charityshop.datamodel.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.PictureData;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
@SuppressWarnings({ "checkstyle:DesignForExtension" })
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;

    private static final int NAME_CELL = 1;
    private static final int CATEGORY_CELL = 2;
    private static final int QUANTITY_CELL = 3;
    private static final int PRICE_CELL = 4;

    @Override
    @Transactional
    public void addProductsFromFile(final File productsFile) {
        List<ProductImportData> imports = getImports(productsFile);
        for (ProductImportData p : imports) {
            Product productDb = new Product();
            productDb.setName(p.getName());
            Optional<Category> category = categoryRepository.findByName(p.getCategory());
            if (category.isPresent()) {
                productDb.setCategory(category.get());
            } else {
                Category categoryDb = new Category();
                categoryDb.setName(p.getCategory());
                categoryRepository.save(categoryDb);
                productDb.setCategory(categoryDb);
            }
            productDb.setPicture(p.getPicture());
            productDb.setQuantity(p.getQuantity());
            productDb.setPrice(p.getPrice());

            productRepository.save(productDb);
            log.debug("Product added {}", productDb);
        }
    }

    @SuppressWarnings("PMD.CloseResource")
    private List<ProductImportData> getImports(final File productsFile) {
        List<ProductImportData> productDataList = new ArrayList<>();
        Workbook workbook;
        try {
            workbook = new XSSFWorkbook(new FileInputStream(productsFile));
        } catch (IOException e) {
            return productDataList;
        }
        Sheet sheet = workbook.getSheetAt(0);
        List pics = workbook.getAllPictures();
        int i = 0;
        for (Row row : sheet) {
            if (i == 0) {
                i++;
                continue;
            }

            ProductImportData productData = new ProductImportData();

            PictureData pict = (PictureData) pics.get(i - 1);
            byte[] picData = pict.getData();
            productData.setPicture(picData);

            DataFormatter formatter = new DataFormatter();
            productData.setName(formatter.formatCellValue(row.getCell(NAME_CELL)));
            productData.setCategory(formatter.formatCellValue(row.getCell(CATEGORY_CELL)));
            productData.setQuantity((int) row.getCell(QUANTITY_CELL).getNumericCellValue());
            String priceString = row.getCell(PRICE_CELL).getNumericCellValue() + "";
            priceString = priceString.replace(",", ".");
            productData.setPrice(new BigDecimal(priceString));

            productDataList.add(productData);
            i++;
        }
        try {
            workbook.close();
        } catch (IOException ignored) { }
        return productDataList;
    }
}
