package com.eljacko.charityshop.webapi;

import com.eljacko.charityshop.webapi.service.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.IOException;

@Slf4j
@Configuration
@RequiredArgsConstructor
@PropertySource(value = "classpath:version.properties", ignoreResourceNotFound = true)
public class ApplicationStartListener implements ApplicationListener<ContextRefreshedEvent> {

    private final ConfigurableApplicationContext context;
    private final WebSecurityConfig webConfig;
    private final ProductService productService;


    @Override
    public final void onApplicationEvent(final ContextRefreshedEvent event) {
        boolean shutDown = false;
        if (log.isInfoEnabled()) {
            log.info("Verifying configuration parameters...");
        }

        if (webConfig.getFrontendPublicUrl() == null
                || webConfig.getFrontendPublicUrl().length() < 2) {
            log.error("There is no web-api.frontend.public-url value in "
                    + "configuration file!");
            shutDown = true;
        }

        try {
            File products = new ClassPathResource("data/products.xlsx").getFile();
            if (!products.isFile()) {
                products = new ClassPathResource("data/products.cls").getFile();
            }
            if (products.isFile()) {
                productService.addProductsFromFile(products);
            }
        } catch (IOException e) {
            log.error("Error loading products list from resources");
        }

        if (shutDown) {
            SpringApplication.exit(context);
        } else {
            if (log.isInfoEnabled()) {
                log.info("Configuration OK");
                log.info("Starting Web API application");
            }
        }

    }
}
