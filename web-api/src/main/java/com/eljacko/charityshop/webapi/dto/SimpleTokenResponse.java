package com.eljacko.charityshop.webapi.dto;

import com.eljacko.charityshop.webapi.dto.response.CommonTokenResponse;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SimpleTokenResponse extends CommonTokenResponse {
}

