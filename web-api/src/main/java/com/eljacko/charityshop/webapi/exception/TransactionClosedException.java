package com.eljacko.charityshop.webapi.exception;

@SuppressWarnings({ "checkstyle:FinalParameters" })
public class TransactionClosedException extends RuntimeException {
    private static final long serialVersionUID = 6076119498371982779L;

    public TransactionClosedException() {
        super();
    }

    public TransactionClosedException(final String message) {
        super(message);
    }

    public TransactionClosedException(final String message, Throwable cause) {
        super(message, cause);
    }

    public TransactionClosedException(Throwable cause) {
        super(cause);
    }
}
