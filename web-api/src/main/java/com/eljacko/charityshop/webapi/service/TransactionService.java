package com.eljacko.charityshop.webapi.service;

import com.eljacko.charityshop.webapi.dto.transaction.TransactionData;
import com.eljacko.charityshop.webapi.dto.transaction.TransactionDataSaveResult;

public interface TransactionService {

    TransactionDataSaveResult save(TransactionData transactionData);

    void processAbandonedCarts();

}
