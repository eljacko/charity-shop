package com.eljacko.charityshop.webapi.tasker;

import com.eljacko.charityshop.webapi.service.TransactionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
@SuppressWarnings({"checkstyle:designforextension", "checkstyle:magicnumber"})
public class AbandonedCartsTasker {

    private final TransactionService transactionService;

    @Scheduled(fixedDelay = 60_000)
    public void markCancelled() {
        transactionService.processAbandonedCarts();
    }
}
