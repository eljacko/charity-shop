package com.eljacko.charityshop.webapi.exception;

@SuppressWarnings({ "checkstyle:FinalParameters" })
public class ApplicationException extends RuntimeException {
    private static final long serialVersionUID = 1407611942571982779L;

    public ApplicationException() {
        super();
    }

    public ApplicationException(final String message) {
        super(message);
    }

    public ApplicationException(final String message, Throwable cause) {
        super(message, cause);
    }

    public ApplicationException(Throwable cause) {
        super(cause);
    }
}
