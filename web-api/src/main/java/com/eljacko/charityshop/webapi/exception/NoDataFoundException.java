package com.eljacko.charityshop.webapi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@SuppressWarnings({ "checkstyle:FinalParameters" })
@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "no_data_found")
public class NoDataFoundException extends RuntimeException {
    private static final long serialVersionUID = 4076119488371982779L;

    public NoDataFoundException() {
        super();
    }

    public NoDataFoundException(final String message) {
        super(message);
    }

    public NoDataFoundException(final String message, Throwable cause) {
        super(message, cause);
    }

    public NoDataFoundException(Throwable cause) {
        super(cause);
    }
}
