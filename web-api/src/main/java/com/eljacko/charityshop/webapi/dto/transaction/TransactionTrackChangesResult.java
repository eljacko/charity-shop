package com.eljacko.charityshop.webapi.dto.transaction;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class TransactionTrackChangesResult {

    private Long id;
    private String checksum;
    private boolean changed;
    /**
     * Represents time in milliseconds when transaction data was changed
     */
    private Long dataChanged;

}
