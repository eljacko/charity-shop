package com.eljacko.charityshop.webapi.dto.response;

import java.time.Instant;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
public abstract class CommonTokenResponse {
    private String timestamp;

    public final void setTimestampAsStr(final Instant time) {
        setTimestamp(time.toString());
    }
}
