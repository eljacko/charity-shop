## Building

Gradle is used for building the project and compiling a distribution archive. 
To build everything:

    ./gradlew build

For running on your computer use, execute subproject task:

    ./gradlew clean build :web-api:bootRun
    
To run the application without first building an archive use the bootRun task:
    
    ./gradlew :web-api:bootRun

To build all spring-boot jars:

    ./gradlew clean bootJar
        
To run data source integration tests:

    ./gradlew clean build :data-model:integrationTest
    
By default, application uses 8088 port, to change port put in application.properties:

	server.port=8088

## Configuration
There should be 2 configurations files to run an application on a developer's computer:

	application.properties (additional)
	log4j2.xml
	
#### Additional `application.properties` file
Default values for the charity shop application are provided in a `application.properties` file in the location `web-api/src/main/resources`. This file is called as **default** `application.properties` file. To run charity shop application you need to create an additional `application.properties` file where you define environment values. This file is called as **additional** `application.properties` file. A template for the additional file is located in `web-api/` directory as `application.properties.additional.sample`, there are the minimum values that you need to run the application. Put your additional `application.properties` file to the `web-api` module root directory to load configuration properties automatically. If there is need to change values defined in the default file, then you can do it by putting them to your additional `application.properties` file. All values are defined in the additional are override values from the the default `application.properties` file.

#### `log4j2.xml`
For logging, you need to create a file `log4j2.xml`. A template for the file is located in `web-api/src/main/resources` `log4j2.xml.sample` Put the created file to the `web-api` module root directory or to `web-api/src/main/resources` to load logging configuration automatically.

### Creating databases
Database creation script `initdb.sh` is also located in `data-model/resources` 

    set -e
    
    psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    
    CREATE USER superuser WITH PASSWORD 'ajutine123';
    CREATE USER web_api WITH PASSWORD 'ajutine123';
    
    CREATE DATABASE charity_shop_db WITH OWNER superuser
        ENCODING 'UTF8' LC_COLLATE 'C' LC_CTYPE 'C' TEMPLATE template0;
    
    \c charity_shop_db
    
    ALTER SCHEMA public OWNER TO superuser;
    
    EOSQL
    
##Tests

###Swagger

Default url for testing with swagger 

    http://localhost:8088/swagger-ui/

###Unit Test Naming Convention

Writing unit tests use the following method name convention:

`methodName_StateUnderTest_ExpectedBehavior`

`methodName` is the name of the method is under test (e.g. `savePerson`)

`StateUnderTest` is the state before running the test. The scenario that's being tested. This could be `WithExistingEmail` indicating that we are saving person with email that already has another person.

`ExpectedBehavior` is the expected result of running the test or the expected return value. In the example above that could be `ThrowEx` indicating that exception should be thrown.

And the method for example above is:

`public final void savePerson_WithExistingEmail_ThrowEx() {...} `

