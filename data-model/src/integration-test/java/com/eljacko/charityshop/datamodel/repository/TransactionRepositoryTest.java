package com.eljacko.charityshop.datamodel.repository;

import com.eljacko.charityshop.datamodel.AbstractIntegrationTest;
import com.eljacko.charityshop.datamodel.entity.transaction.Transaction;
import com.eljacko.charityshop.datamodel.utils.ForeignEntityBuilder;
import lombok.Getter;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.beans.SamePropertyValuesAs.samePropertyValuesAs;
import static org.junit.Assert.assertThat;

@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@SuppressWarnings({ "checkstyle:MethodName" })
public class TransactionRepositoryTest extends AbstractIntegrationTest {
    @Autowired
    private TransactionRepository repository;
    @Getter
    @Autowired
    private TestEntityManager entityManager;

    private transient Transaction globalTransaction;

    private void saveTransaction() {
        globalTransaction = ForeignEntityBuilder.getTransaction();
        repository.save(globalTransaction);
        entityManager.flush();
    }

    @Test
    public final void findById_returnOne() {
        saveTransaction();
        Optional<Transaction> actual = repository.findById(globalTransaction.getId());
        assertThat(actual.get(), samePropertyValuesAs(globalTransaction));
    }

    @Test
    public final void findById_returnNull() {
        Optional<Transaction> fromDb = repository.findById(getRunId());
        assertThat(fromDb.isPresent(), is(false));
    }

    @Test
    public final void save_requiredData() {
        Transaction transaction = ForeignEntityBuilder.getTransaction();
        Transaction savedTransaction = repository.save(transaction);
        entityManager.flush();
        assertThat(transaction, samePropertyValuesAs(savedTransaction));
    }

}
