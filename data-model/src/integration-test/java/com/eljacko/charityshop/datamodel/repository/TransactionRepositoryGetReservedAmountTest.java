package com.eljacko.charityshop.datamodel.repository;

import com.eljacko.charityshop.datamodel.AbstractIntegrationTest;
import com.eljacko.charityshop.datamodel.entity.product.Product;
import com.eljacko.charityshop.datamodel.entity.transaction.Transaction;
import com.eljacko.charityshop.datamodel.entity.transaction.TransactionDetails;
import com.eljacko.charityshop.datamodel.utils.UnitTestHelper;
import lombok.Getter;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@SuppressWarnings({"checkstyle:MethodName", "checkstyle:MagicNumber"})
public class TransactionRepositoryGetReservedAmountTest extends AbstractIntegrationTest {
    @Autowired
    private TransactionRepository repository;
    @Getter
    @Autowired
    private TestEntityManager entityManager;

    @Test
    public final void getReservedAmount_VerifySqlWithoutData_Zero() {
        flushAndClear();
        Integer amount = repository.getReservedAmount(1L);
        assertThat(amount, is(0));
    }

    @Test
    public final void getReservedAmount_OneOpenTransaction_ReturnTransactionQuantity() {
        flushAndClear();
        Product product = getInsertedProduct(null);
        Transaction transaction = getInsertedTransaction(null);
        TransactionDetails transactionDetails = getInsertedTransactionDetails(transaction,
                product, null);
        transactionDetails.setQuantity(2);
        Integer amount = repository.getReservedAmount(product.getId());
        assertThat(amount, is(2));
    }

    @Test
    public final void getReservedAmount_OneClosedTransaction_Zero() {
        Product product = getInsertedProduct(null);
        Transaction transaction = getInsertedTransaction(null);
        transaction.setPaymentReceived(UnitTestHelper.currentTimeAsTimestamp());
        TransactionDetails transactionDetails = getInsertedTransactionDetails(transaction,
                product, null);
        transactionDetails.setQuantity(2);
        flushAndClear();

        Integer amount = repository.getReservedAmount(product.getId());
        assertThat(amount, is(0));
    }

    @Test
    public final void getReservedAmount_OneCancelledTransaction_Zero() {
        Product product = getInsertedProduct(null);
        Transaction transaction = getInsertedTransaction(null);
        transaction.setCancelled(UnitTestHelper.currentTimeAsTimestamp());
        TransactionDetails transactionDetails = getInsertedTransactionDetails(transaction,
                product, null);
        transactionDetails.setQuantity(2);
        flushAndClear();

        Integer amount = repository.getReservedAmount(product.getId());
        assertThat(amount, is(0));
    }

    @Test
    public final void getReservedAmount_MultipleOpenTransactions_ReturnSum() {
        flushAndClear();
        Product product = getInsertedProduct(null);
        Transaction transaction = getInsertedTransaction(null);
        TransactionDetails transactionDetails = getInsertedTransactionDetails(transaction,
                product, null);
        transactionDetails.setQuantity(2);
        Transaction transaction2 = getInsertedTransaction(null);
        TransactionDetails transactionDetails2 = getInsertedTransactionDetails(transaction2,
                product, null);
        transactionDetails2.setQuantity(1);
        Integer amount = repository.getReservedAmount(product.getId());
        assertThat(amount, is(3));
    }

    @Test
    public final void getReservedAmount_OneOpenOneClosedTransactions_ReturnOpenQuantity() {
        flushAndClear();
        Product product = getInsertedProduct(null);
        Transaction transaction = getInsertedTransaction(null);
        TransactionDetails transactionDetails = getInsertedTransactionDetails(transaction,
                product, null);
        transactionDetails.setQuantity(2);
        Transaction transaction2 = getInsertedTransaction(null);
        TransactionDetails transactionDetails2 = getInsertedTransactionDetails(transaction2,
                product, null);
        transactionDetails2.setQuantity(1);
        transaction2.setPaymentReceived(UnitTestHelper.currentTimeAsTimestamp());
        Integer amount = repository.getReservedAmount(product.getId());
        assertThat(amount, is(2));
    }
}
