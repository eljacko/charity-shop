package com.eljacko.charityshop.datamodel.repository;

import com.eljacko.charityshop.datamodel.AbstractIntegrationTest;
import com.eljacko.charityshop.datamodel.constant.field.ProductListItemField;
import com.eljacko.charityshop.datamodel.constant.sort.ProductListSortingKeys;
import com.eljacko.charityshop.datamodel.dto.PageResultWrapper;
import com.eljacko.charityshop.datamodel.dto.product.ProductListItem;
import com.eljacko.charityshop.datamodel.entity.product.Product;
import lombok.Getter;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@SuppressWarnings({ "checkstyle:MethodName", "checkstyle:magicnumber" })
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class ProductRepositoryGetProductsTest extends AbstractIntegrationTest {
    @Autowired
    private ProductRepository repository;
    @Getter
    @Autowired
    private TestEntityManager entityManager;

    @Test
    public final void getProducts_VerifySqlWithoutData_Empty() {
        Set<ProductListItemField> fields = new HashSet<>(3);
        fields.add(ProductListItemField.ID);
        fields.add(ProductListItemField.NAME);
        fields.add(ProductListItemField.QUANTITY);
        flushAndClear();
        PageResultWrapper<ProductListItem> result = repository.getProducts(getPageRequest(),
                getPageRequest().getSort(), fields);
        assertThat(result.getTotal(), is(0L));
        assertThat(result.getResults(), empty());
    }

    @Test
    public final void getProducts_NoPaging_Empty() {
        Set<ProductListItemField> fields = getAllFields();
        flushAndClear();
        PageResultWrapper<ProductListItem> result = repository.getProducts(null, null, fields);
        assertThat(result.getTotal(), is(0L));
        assertThat(result.getResults(), empty());
    }

    @Test
    public final void getProducts_OneProduct_ListOfOne() {
        Set<ProductListItemField> fields = getAllFields();
        Product product = getInsertedProduct(null);
        flushAndClear();
        PageResultWrapper<ProductListItem> result = repository.getProducts(null,
                getPageRequest().getSort(), fields);

        assertThat(result.getResults(), hasSize(1));
        assertThat(result.getTotal(), is(1L));
        ProductListItem actualProduct = result.getResults().iterator().next();
        assertThat(actualProduct.getId(), is(product.getId()));
        assertThat(actualProduct.getName(), is(product.getName()));
    }

    @Test
    public final void getProducts_ManyProduct_List() {
        Set<ProductListItemField> fields = getAllFields();
        Product product1 = getInsertedProduct(p -> p.setName("test product 1"));
        Product product2 = getInsertedProduct(p -> {
            p.setName("test product 1");
            p.setQuantity(2);
        });
        flushAndClear();

        ProductListItem productItem1 = new ProductListItem(product1.getId(), product1.getName());
        ProductListItem productItem2 = new ProductListItem(product2.getId(), product2.getName());
        productItem2.setQuantity(2);
        PageResultWrapper<ProductListItem> result = repository.getProducts(null,
                getPageRequest().getSort(), fields);
        assertThat(result.getResults(), hasSize(2));
        assertThat(result.getTotal(), is(2L));
        assertThat(result.getResults(), hasItem(productItem1));
        assertThat(result.getResults(), hasItem(productItem2));
    }

    @Test
    public final void getProducts_SortedResult_ReturnList() {
        Set<ProductListItemField> fields = getAllFields();
        final int insertedItems = 9;

        IntStream.rangeClosed(1, insertedItems).map(i -> insertedItems - i).forEach(i -> {
            getInsertedProduct(currProduct -> {
                currProduct.setName("Product " + i);
            });
        });
        flushAndClear();

        PageResultWrapper<ProductListItem> result = repository.getProducts(null,
                getPageRequest().getSort(), fields);
        checkSortRersults(insertedItems, result);
    }

    private void checkSortRersults(final int insertedItems,
                                   final PageResultWrapper<ProductListItem> result) {
        assertThat(result.getResults(), hasSize(insertedItems));
        assertThat(result.getTotal(), is(Long.valueOf(insertedItems)));
        int currEl = 0;
        for (ProductListItem actualItem : result.getResults()) {
            assertThat(actualItem.getName(), is("Product " + currEl));
            currEl++;
        }
    }

    @Test
    public final void getProducts_PagedResult_ListWithPageSize() {
        Set<ProductListItemField> fields = getAllFields();
        final int insertedItems = 9;
        IntStream.rangeClosed(1, insertedItems).forEach(i -> {
            getInsertedProduct(null);
        });
        flushAndClear();
        PageRequest pageRequest = PageRequest.of(0, 5);
        PageResultWrapper<ProductListItem> result = repository.getProducts(pageRequest,
                null, fields);
        assertThat(result.getResults(), hasSize(getPageRequest().getPageSize()));
        assertThat(result.getTotal(), is(Long.valueOf(insertedItems)));
    }

    private PageRequest getPageRequest() {
        return PageRequest.of(0, 5, getProductSort());
    }

    private Sort getProductSort() {
        return Sort.by(Arrays.asList(
                new Sort.Order(Sort.Direction.ASC,
                        ProductListSortingKeys.CATEGORY),
                new Sort.Order(Sort.Direction.ASC, ProductListSortingKeys.NAME)));

    }

    private Set<ProductListItemField> getAllFields() {
        return Stream.of(ProductListItemField.values()).collect(Collectors.toSet());
    }
}
