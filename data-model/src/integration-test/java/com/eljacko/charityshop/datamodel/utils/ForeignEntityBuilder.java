package com.eljacko.charityshop.datamodel.utils;

import com.eljacko.charityshop.datamodel.entity.product.Category;
import com.eljacko.charityshop.datamodel.entity.product.Product;
import com.eljacko.charityshop.datamodel.entity.transaction.Transaction;
import com.eljacko.charityshop.datamodel.entity.transaction.TransactionDetails;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.function.Consumer;

@SuppressWarnings({ "checkstyle:magicnumber" })
public final class ForeignEntityBuilder {
 // @formatter:off
    public static final String ALPHABET =
            "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
 // @formatter:on
    public static Long getRunId() {
        return new Long(new SimpleDateFormat("yyyMMddHHmmssS").format(new Date()));
    }

    public static Product getProduct() {
        return getProduct(null);
    }

    public static Product getProduct(final Consumer<Product> dataSetter) {
        final Product product = new Product();
        if (dataSetter != null) {
            dataSetter.accept(product);
        }
        return product;
    }

    public static Category getCategory() {
        final Category category = new Category();
        category.setName(
                StringTestUtil.randomStringWithLength(10) + "_" + ForeignEntityBuilder.getRunId());
        return category;
    }

    public static Category getCategory(final String name, final Consumer<Category> dataSetter) {
        final Category category = new Category();
        if (StringUtils.isEmpty(name)) {
            category.setName(StringTestUtil.randomStringWithLength(50) + "_"
                    + ForeignEntityBuilder.getRunId());
        } else {
            category.setName(name);
        }
        if (dataSetter != null) {
            dataSetter.accept(category);
        }
        return category;
    }

    public static Transaction getTransaction() {
        return getTransaction(null);
    }

    public static Transaction getTransaction(
            final Consumer<Transaction> dataSetter) {
        final Transaction transaction = new Transaction();
        transaction.setLastAction(UnitTestHelper.currentTimeAsTimestamp());
        if (dataSetter != null) {
            dataSetter.accept(transaction);
        }
        return transaction;
    }

    public static TransactionDetails getTransactionDetails(final Transaction transaction,
            final Product product, final Consumer<TransactionDetails> dataSetter) {
        final TransactionDetails transactionDetails = new TransactionDetails();
        transactionDetails.setTransaction(transaction);
        transactionDetails.setProduct(product);
        if (dataSetter != null) {
            dataSetter.accept(transactionDetails);
        }
        return transactionDetails;
    }

    private ForeignEntityBuilder() {
        throw new UnsupportedOperationException(
                "This is a utils class and cannot be instantiated");
    }
}
