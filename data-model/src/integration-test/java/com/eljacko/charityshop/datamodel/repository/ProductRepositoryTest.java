package com.eljacko.charityshop.datamodel.repository;

import com.eljacko.charityshop.datamodel.AbstractIntegrationTest;
import com.eljacko.charityshop.datamodel.entity.product.Product;
import com.eljacko.charityshop.datamodel.utils.ForeignEntityBuilder;
import lombok.Getter;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.beans.SamePropertyValuesAs.samePropertyValuesAs;
import static org.junit.Assert.assertThat;

@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@SuppressWarnings({ "checkstyle:MethodName" })
public class ProductRepositoryTest extends AbstractIntegrationTest {
    @Autowired
    private ProductRepository repository;
    @Getter
    @Autowired
    private TestEntityManager entityManager;

    private transient Product globalProduct;

    private void saveProduct() {
        globalProduct = ForeignEntityBuilder.getProduct();
        repository.save(globalProduct);
        entityManager.flush();
    }

    @Test
    public final void findById_returnOne() {
        saveProduct();
        Optional<Product> actual = repository.findById(globalProduct.getId());
        assertThat(actual.get(), samePropertyValuesAs(globalProduct));
    }

    @Test
    public final void findById_returnNull() {
        Optional<Product> fromDb = repository.findById(getRunId());
        assertThat(fromDb.isPresent(), is(false));
    }

    @Test
    public final void save_requiredData() {
        Product product = ForeignEntityBuilder.getProduct();
        Product savedProduct = repository.save(product);
        entityManager.flush();
        assertThat(product, samePropertyValuesAs(savedProduct));
    }
}
