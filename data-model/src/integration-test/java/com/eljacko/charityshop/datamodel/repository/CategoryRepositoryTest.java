package com.eljacko.charityshop.datamodel.repository;

import com.eljacko.charityshop.datamodel.AbstractIntegrationTest;
import com.eljacko.charityshop.datamodel.entity.product.Category;
import com.eljacko.charityshop.datamodel.utils.ForeignEntityBuilder;
import lombok.Getter;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.beans.SamePropertyValuesAs.samePropertyValuesAs;
import static org.junit.Assert.assertThat;

@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@SuppressWarnings({ "checkstyle:MethodName" })
public class CategoryRepositoryTest extends AbstractIntegrationTest {
    @Autowired
    private CategoryRepository repository;
    @Getter
    @Autowired
    private TestEntityManager entityManager;

    private transient Category globalCategory;

    private void saveCategory() {
        globalCategory = ForeignEntityBuilder.getCategory();
        repository.save(globalCategory);
        entityManager.flush();
    }

    @Test
    public final void findById_returnOne() {
        saveCategory();
        Optional<Category> actual = repository.findById(globalCategory.getId());
        assertThat(actual.get(), samePropertyValuesAs(globalCategory));
    }

    @Test
    public final void findByName_foundOne() {
        saveCategory();
        Optional<Category> actual = repository.findByName(globalCategory.getName());
        assertThat(actual.get(), is(globalCategory));
    }

    @Test
    public final void findById_returnNull() {
        Optional<Category> fromDb = repository.findById(getRunId());
        assertThat(fromDb.isPresent(), is(false));
    }

    @Test
    public final void findByName_NotFound() {
        saveCategory();
        Optional<Category> actual = repository.findByName(getRunId().toString());
        assertThat(actual.isPresent(), is(false));
    }

    @Test
    public final void save_requiredData() {
        Category category = ForeignEntityBuilder.getCategory();
        Category savedCategory = repository.save(category);
        entityManager.flush();
        assertThat(category, samePropertyValuesAs(savedCategory));
    }

}
