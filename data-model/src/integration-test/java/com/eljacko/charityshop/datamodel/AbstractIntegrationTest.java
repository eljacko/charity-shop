package com.eljacko.charityshop.datamodel;


import com.eljacko.charityshop.datamodel.entity.product.Product;
import com.eljacko.charityshop.datamodel.entity.transaction.Transaction;
import com.eljacko.charityshop.datamodel.entity.transaction.TransactionDetails;
import com.eljacko.charityshop.datamodel.utils.ForeignEntityBuilder;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.function.Consumer;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@SuppressWarnings({ "checkstyle:magicnumber" })
public abstract class AbstractIntegrationTest {
    public abstract TestEntityManager getEntityManager();

    protected final Long getRunId() {
        return new Long(new SimpleDateFormat("yyyMMddHHmmssS").format(new Date()));
    }

    protected final void flushAndClear() {
        getEntityManager().flush();
        getEntityManager().clear();
    }

    protected final Product getInsertedProduct(final Consumer<Product> dataSetter) {
        final Product productDb = getEntityManager()
                .persistFlushFind(ForeignEntityBuilder.getProduct(dataSetter));
        getEntityManager().flush();
        return productDb;
    }

    protected final Transaction getInsertedTransaction(final Consumer<Transaction> dataSetter) {
        final Transaction transactionDb = getEntityManager()
                .persistFlushFind(ForeignEntityBuilder.getTransaction(dataSetter));
        getEntityManager().flush();
        return transactionDb;
    }

    protected final TransactionDetails getInsertedTransactionDetails(final Transaction transaction,
                                             final Product product,
                                             final Consumer<TransactionDetails> detailsDataSetter) {
        TransactionDetails retDetails = ForeignEntityBuilder.getTransactionDetails(transaction,
                product, detailsDataSetter);
        retDetails = getEntityManager().persistFlushFind(retDetails);
        return retDetails;
    }
}
