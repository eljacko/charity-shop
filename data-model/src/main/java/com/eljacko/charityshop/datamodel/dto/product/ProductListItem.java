package com.eljacko.charityshop.datamodel.dto.product;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode
@NoArgsConstructor
public class ProductListItem {

    private Long id;
    private String name;
    private String category;
    private BigDecimal price;
    private Integer quantity;
    private Integer reservedQuantity;
    @ToString.Exclude
    private String picture;

    public ProductListItem(final Long id, final String name) {
        this.id = id;
        this.name = name;
    }
}
