package com.eljacko.charityshop.datamodel.entity.transaction;

import com.eljacko.charityshop.datamodel.entity.base.BaseEntity;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.sql.Timestamp;
import java.util.Set;

@Setter
@ToString(callSuper = true)
@Entity
@Table(name = "transaction")
//@formatter:off
@GenericGenerator(
        name = "assigned-sequence",
        strategy = "com.eljacko.charityshop.datamodel.utils.AssignedSequenceStyleGenerator",
        parameters = {
                @org.hibernate.annotations.Parameter(
                        name = "sequence_name",
                        value = "transactions_id_seq"),
                @org.hibernate.annotations.Parameter(
                        name = "optimizer",
                        value = "none")
        })
//@formatter:on
@SuppressWarnings({ "checkstyle:DesignForExtension" })
public class Transaction extends BaseEntity {
    private static final long serialVersionUID = 7633033877550598798L;

    private Set<TransactionDetails> transactionDetails;
    private String note;
    // Filled when the payment is received, used as a status flag
    private Timestamp paymentReceived;
    // Indicates the timestamp of the last status change of an transaction
    private Timestamp lastAction;
    // Filled when the transaction is cancelled or timed out, used as a status flag
    private Timestamp cancelled;


    public Transaction() {
        super();
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "transaction")
    public Set<TransactionDetails> getTransactionDetails() {
        return transactionDetails;
    }

    public String getNote() {
        return note;
    }

    @Column
    public Timestamp getPaymentReceived() {
        return paymentReceived;
    }

    @Column(nullable = false)
    public Timestamp getLastAction() {
        return lastAction;
    }

    @Column
    public Timestamp getCancelled() {
        return cancelled;
    }

    @Transient
    public void setTransactionDetails(final Set<TransactionDetails> transactionDetails) {
        this.transactionDetails = transactionDetails;
    }
}
