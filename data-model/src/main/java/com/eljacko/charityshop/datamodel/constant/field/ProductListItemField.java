package com.eljacko.charityshop.datamodel.constant.field;

import lombok.Getter;

public enum ProductListItemField {
    // @formatter:off
    ID("id"),
    NAME("name"),
    PRICE("price"),
    CATEGORY("category"),
    QUANTITY("quantity"),
    RESERVED_QUANTITY("reservedQuantity"),
    PICTURE("picture");
    // @formatter:on

    @Getter
    private String text;

    ProductListItemField(final String text) {
        this.text = text;
    }

    public static ProductListItemField fromString(final String text) {
        for (ProductListItemField b : ProductListItemField.values()) {
            if (b.text.equalsIgnoreCase(text)) {
                return b;
            }
        }
        throw new IllegalArgumentException("No field with name '" + text + "' found");
    }

    @Override
    public String toString() {
        return text;
    }
}
