package com.eljacko.charityshop.datamodel.repository.product;

import com.eljacko.charityshop.datamodel.constant.field.ProductListItemField;
import com.eljacko.charityshop.datamodel.constant.sort.ProductListSortingKeys;
import com.eljacko.charityshop.datamodel.dto.PageResultWrapper;
import com.eljacko.charityshop.datamodel.dto.product.ProductListItem;
import com.eljacko.charityshop.datamodel.entity.product.Category;
import com.eljacko.charityshop.datamodel.entity.product.Category_;
import com.eljacko.charityshop.datamodel.entity.product.Product;
import com.eljacko.charityshop.datamodel.entity.product.Product_;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;
import javax.persistence.metamodel.SingularAttribute;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@Slf4j
public class ProductRepositoryImpl implements ProductRepositoryCustom {
    @PersistenceContext
    private EntityManager em;

    @Override
    public final PageResultWrapper<ProductListItem> getProducts(
            final PageRequest pageRequest, final Sort sort,
            final @NotNull Set<ProductListItemField> fields) {
        Assert.notEmpty(fields, "Fields must not be null");
        PageResultWrapper<ProductListItem> result = new PageResultWrapper<>();
        Long count;
        TypedQuery<Tuple> q = getQueryForItems(sort, Tuple.class, fields);
        if (pageRequest != null && (pageRequest.getOffset() > 0 || pageRequest.getPageSize() > 0)) {
            TypedQuery<Long> totalQuery = getQueryForItems(null, Long.class, fields);
            count = totalQuery.getSingleResult();
            if (log.isDebugEnabled()) {
                log.debug("Total found {}", count);
            }
            q.setFirstResult(Math.toIntExact(pageRequest.getOffset()))
                    .setMaxResults(pageRequest.getPageSize());
            result.setTotal(count);
        }
        List<ProductListItem> productDtoList;
        List<Tuple> tupleResult = q.getResultList();
        if (CollectionUtils.isEmpty(tupleResult)) {
            productDtoList = Collections.emptyList();
        } else {
            productDtoList = new ArrayList<>(tupleResult.size());
            for (Tuple t : tupleResult) {
                ProductListItem dto = new ProductListItem();
                if (fields.contains(ProductListItemField.ID)) {
                    dto.setId((Long) t.get(ProductListItemField.ID.getText()));
                }
                if (fields.contains(ProductListItemField.NAME)) {
                    dto.setName((String) t.get(ProductListItemField.NAME.getText()));
                }
                if (fields.contains(ProductListItemField.PICTURE)) {
                    byte[] pictureBites = (byte[]) t.get(ProductListItemField
                            .PICTURE.getText());
                    if (pictureBites != null  && pictureBites.length > 0) {
                        dto.setPicture(Base64.getEncoder()
                                .encodeToString(pictureBites));
                    }
                }
                if (fields.contains(ProductListItemField.CATEGORY)) {
                    dto.setCategory((String) t.get(ProductListItemField.CATEGORY.getText()));
                }
                if (fields.contains(ProductListItemField.PRICE)) {
                    dto.setPrice((BigDecimal) t.get(ProductListItemField.PRICE.getText()));
                }
                if (fields.contains(ProductListItemField.QUANTITY)) {
                    dto.setQuantity((Integer) t.get(ProductListItemField.QUANTITY.getText()));
                }
                productDtoList.add(dto);
            }
        }
        result.setResults(productDtoList);
        if (result.getTotal() == null) {
            result.setTotal(Long.valueOf(result.getResults().size()));
        }
        return result;
    }

    @SuppressWarnings({ "unchecked", "checkstyle:MethodLength" })
    private <T> TypedQuery<T> getQueryForItems(
            final org.springframework.data.domain.Sort sort,
            final Class<T> clazz,
            final @NotNull Set<ProductListItemField> fields) {
        CriteriaBuilder cb = getEm().getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = cb.createQuery(clazz);
        Root<Product> productTable = criteriaQuery.from(Product.class);
        productTable.alias("productTable");

        final Join<Product, Category> productCategoryJoin = productTable
                .join(Product_.category, JoinType.LEFT);
        productCategoryJoin.alias("prodCat");

        if (clazz.equals(Long.class)) {
            criteriaQuery.select((Selection<? extends T>) cb.count(productTable));
        } else {
            List<Selection<?>> multiSelection = new ArrayList<>();
            if (fields.contains(ProductListItemField.ID)) {
                multiSelection.add(productTable.get(Product_.id)
                        .alias(ProductListItemField.ID.getText()));
            }
            if (fields.contains(ProductListItemField.NAME)) {
                multiSelection.add(productTable.get(Product_.name)
                        .alias(ProductListItemField.NAME.getText()));
            }
            if (fields.contains(ProductListItemField.PRICE)) {
                multiSelection.add(productTable.get(Product_.price)
                        .alias(ProductListItemField.PRICE.getText()));
            }
            if (fields.contains(ProductListItemField.PICTURE)) {
                multiSelection.add(productTable.get(Product_.picture)
                        .alias(ProductListItemField.PICTURE.getText()));
            }
            if (fields.contains(ProductListItemField.QUANTITY)) {
                multiSelection.add(productTable.get(Product_.quantity)
                        .alias(ProductListItemField.QUANTITY.getText()));
            }
            if (fields.contains(ProductListItemField.CATEGORY)) {
                multiSelection.add(productCategoryJoin.get(Category_.name)
                        .alias(ProductListItemField.CATEGORY.getText()));
            }
            criteriaQuery.multiselect(multiSelection);
        }

        List<javax.persistence.criteria.Order> orderByList = new ArrayList<>();
        if (sort != null) {
            sort.forEach(order -> {
                if (order.getProperty() != null) {
                    SingularAttribute<? extends Object, ? extends Object> field = null;
                    Path<? extends Object> table = null;
                    switch (order.getProperty().toLowerCase()) {
                        case ProductListSortingKeys.ID:
                            field = Product_.id;
                            table = productTable;
                            break;
                        case ProductListSortingKeys.NAME:
                            field = Product_.name;
                            table = productTable;
                            break;
                        case ProductListSortingKeys.PRICE:
                            field = Product_.price;
                            table = productTable;
                            break;
                        case ProductListSortingKeys.QUANTITY:
                            field = Product_.quantity;
                            table = productTable;
                            break;
                        case ProductListSortingKeys.CATEGORY:
                            field = Category_.name;
                            table = productCategoryJoin;
                            break;
                        default:
                            if (log.isWarnEnabled()) {
                                log.warn("{} is unkown property for order by", order.getProperty());
                            }
                            return;
                    }
                    if (field != null) {
                        if (Sort.Direction.DESC.equals(order.getDirection())) {
                            orderByList.add(cb.desc(table.get(field.getName())));
                        } else if (Sort.Direction.ASC.equals(order.getDirection())) {
                            orderByList.add(cb.asc(table.get(field.getName())));
                        }
                    }
                }
            });
        }
        criteriaQuery.orderBy(orderByList);
        return getEm().createQuery(criteriaQuery);
    }
}
