package com.eljacko.charityshop.datamodel.entity.product;

import com.eljacko.charityshop.datamodel.constant.FieldsLength;
import com.eljacko.charityshop.datamodel.constant.ValidationMessages;
import com.eljacko.charityshop.datamodel.entity.base.BaseEntity;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@Table(name = "category")
@DynamicUpdate
@DynamicInsert
@Setter
@ToString(callSuper = true, exclude = "products")
//@formatter:off
@GenericGenerator(
        name = "assigned-sequence",
        strategy = "com.eljacko.charityshop.datamodel.utils.AssignedSequenceStyleGenerator",
        parameters = {
                @org.hibernate.annotations.Parameter(
                        name = "sequence_name",
                        value = "category_id_seq"),
                @org.hibernate.annotations.Parameter(
                        name = "optimizer",
                        value = "none")
        })
//@formatter:on
@SuppressWarnings({ "checkstyle:DesignForExtension" })
public class Category extends BaseEntity {
    private static final long serialVersionUID = 1549505726133603361L;

    private String name;
    private Set<Product> products;

    public Category() {
        super();
    }

    @Size(max = FieldsLength.CATEGORY_NAME, message = ValidationMessages.MAX_LENGTH_EXCEEDED)
    @Column(length = FieldsLength.CATEGORY_NAME)
    public String getName() {
        return name;
    }

    @OneToMany(mappedBy = "category", cascade = CascadeType.ALL)
    public Set<Product> getProducts() {
        return products;
    }

}
