package com.eljacko.charityshop.datamodel.entity.transaction;

import com.eljacko.charityshop.datamodel.entity.base.BaseEntity;
import com.eljacko.charityshop.datamodel.entity.product.Product;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;

@Setter
@ToString(callSuper = true)
@Entity
@Table(name = "transaction_details")
//@formatter:off
@GenericGenerator(
        name = "assigned-sequence",
        strategy = "com.eljacko.charityshop.datamodel.utils.AssignedSequenceStyleGenerator",
        parameters = {
                @org.hibernate.annotations.Parameter(
                        name = "sequence_name",
                        value = "transaction_details_id_seq"),
                @org.hibernate.annotations.Parameter(
                        name = "optimizer",
                        value = "none")
        })
//@formatter:on
@SuppressWarnings({ "checkstyle:DesignForExtension" })
public class TransactionDetails extends BaseEntity {
    private static final long serialVersionUID = 7533033877550591198L;

    private Transaction transaction;
    private Product product;
    private int quantity;
    private BigDecimal price;

    public TransactionDetails() {
        super();
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "transaction_id", nullable = false)
    public Transaction getTransaction() {
        return transaction;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_id", nullable = false)
    public Product getProduct() {
        return product;
    }

    @Column(name = "quantity", nullable = false)
    public int getQuantity() {
        return quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

}
