package com.eljacko.charityshop.datamodel.repository.product;

import com.eljacko.charityshop.datamodel.constant.field.ProductListItemField;
import com.eljacko.charityshop.datamodel.dto.PageResultWrapper;
import com.eljacko.charityshop.datamodel.dto.product.ProductListItem;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.util.Set;

public interface ProductRepositoryCustom {

    PageResultWrapper<ProductListItem> getProducts(PageRequest pageRequest, Sort sort,
                                                   Set<ProductListItemField> fields);

}
