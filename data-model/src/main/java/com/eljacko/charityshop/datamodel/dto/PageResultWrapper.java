package com.eljacko.charityshop.datamodel.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class PageResultWrapper<T> {
    private Long total;
    private List<T> results;
}
