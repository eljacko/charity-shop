package com.eljacko.charityshop.datamodel.repository;

import com.eljacko.charityshop.datamodel.entity.transaction.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    // @formatter:off
    @Query(nativeQuery = true, value = "SELECT COALESCE((SELECT SUM(td.quantity)"
            + " FROM transaction_details td "
            + "     LEFT JOIN transaction t ON td.transaction_id = t.id "
            + " WHERE td.product_id = :productId "
            + "     AND t.payment_received IS NULL "
            + "     AND t.cancelled IS NULL), 0) AS reserved")
    // @formatter:on
    Integer getReservedAmount(@Param("productId") Long productId);

    @Modifying
    // @formatter:off
    @Query("UPDATE Transaction t "
            + " SET t.cancelled = :currentTimestamp "
            + " WHERE t.lastAction < :oldestAllowed "
            + " AND t.cancelled is null"
            + " AND t.paymentReceived is null")
    // @formatter:on
    void cancelAbandoned(@Param("oldestAllowed") Timestamp oldestAllowed,
                         @Param("currentTimestamp") Timestamp currentTimestamp);
}
