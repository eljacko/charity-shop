package com.eljacko.charityshop.datamodel.constant.sort;

import java.util.HashSet;
import java.util.Set;

public final class ProductListSortingKeys {
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String PRICE = "price";
    public static final String CATEGORY = "category";
    public static final String QUANTITY = "quantity";


    private ProductListSortingKeys() {
        throw new UnsupportedOperationException(
                "This is a constants class and cannot be instantiated");
    }

    public static boolean isCorrectSortingKey(final String key) {
        switch (key.toLowerCase()) {
            case ID:
            case NAME:
            case PRICE:
            case CATEGORY:
            case QUANTITY:
                return true;
            default:
                break;
            }
            return false;
        }

    @SuppressWarnings({ "checkstyle:magicnumber" })
    public static Set<String> values() {
        return new HashSet<String>(5) { {
                add(ID);
                add(NAME);
                add(PRICE);
                add(CATEGORY);
                add(QUANTITY);
        } };
    }
}
