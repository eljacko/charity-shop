package com.eljacko.charityshop.datamodel.entity.product;

import com.eljacko.charityshop.datamodel.constant.FieldsLength;
import com.eljacko.charityshop.datamodel.constant.ValidationMessages;
import com.eljacko.charityshop.datamodel.entity.base.BaseEntity;
import com.eljacko.charityshop.datamodel.entity.transaction.TransactionDetails;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Set;

@Setter
@ToString(callSuper = true)
@Entity
@Table(name = "product")
//@formatter:off
@GenericGenerator(
        name = "assigned-sequence",
        strategy = "com.eljacko.charityshop.datamodel.utils.AssignedSequenceStyleGenerator",
        parameters = {
                @org.hibernate.annotations.Parameter(
                        name = "sequence_name",
                        value = "products_id_seq"),
                @org.hibernate.annotations.Parameter(
                        name = "optimizer",
                        value = "none")
        })
//@formatter:on
@SuppressWarnings({ "checkstyle:DesignForExtension", "checkstyle:MagicNumber" })
public class Product extends BaseEntity {
    private static final long serialVersionUID = 7533033877550598198L;

    private String name;
    // as products are loaded by sales event from file,
    // saving price in the product table
    private BigDecimal price;
    @ToString.Exclude
    private byte[] picture;
    private Category category;
    private Integer quantity;
    @ToString.Exclude
    private Set<TransactionDetails> transactionDetails;


    public Product() {
        super();
    }

    @Size(max = FieldsLength.PRODUCT_NAME, message = ValidationMessages.MAX_LENGTH_EXCEEDED)
    @Column(length = FieldsLength.PRODUCT_NAME)
    public String getName() {
        return name;
    }

    @Column(precision = 10, scale = 2)
    @Type(type = "big_decimal")
    public BigDecimal getPrice() {
        return price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    @ManyToOne
    @JoinColumn(name = "category_id")
    public Category getCategory() {
        return category;
    }

    @Type(type = "org.hibernate.type.BinaryType")
    public byte[] getPicture() {
        if (picture != null) {
            return Arrays.copyOf(picture, picture.length);
        } else {
            return null;
        }
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "product")
    public Set<TransactionDetails> getTransactionDetails() {
        return transactionDetails;
    }
}
