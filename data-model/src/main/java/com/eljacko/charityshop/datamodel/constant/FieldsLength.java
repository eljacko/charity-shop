package com.eljacko.charityshop.datamodel.constant;

public final class FieldsLength {

    public static final int CATEGORY_NAME = 64;
    public static final int PRODUCT_NAME = 64;

    private FieldsLength() {
        throw new UnsupportedOperationException(
                "This is a constants class and cannot be instantiated");
    }
}
