package com.eljacko.charityshop.datamodel.hibernate.dialect;

import com.eljacko.charityshop.datamodel.hibernate.type.JsonbType;
import org.hibernate.dialect.PostgreSQL95Dialect;

import java.sql.Types;

public class PostgreSQL95JsonDialect extends PostgreSQL95Dialect {
    public PostgreSQL95JsonDialect() {
        super();
        this.registerHibernateType(
                Types.OTHER, JsonbType.class.getName()
        );
    }
}
