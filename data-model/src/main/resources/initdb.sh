#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL

CREATE USER superuser WITH PASSWORD 'ajutine123';
CREATE USER web_api WITH PASSWORD 'ajutine123';

CREATE DATABASE charity_shop_db WITH OWNER superuser
    ENCODING 'UTF8' LC_COLLATE 'C' LC_CTYPE 'C' TEMPLATE template0;

\c charity_shop_db

ALTER SCHEMA public OWNER TO superuser;

EOSQL
