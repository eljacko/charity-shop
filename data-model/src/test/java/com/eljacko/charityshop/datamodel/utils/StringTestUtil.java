package com.eljacko.charityshop.datamodel.utils;

import java.util.Random;

public final class StringTestUtil {
 // @formatter:off
    public static final String ALPHABET =
            "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
 // @formatter:on
    private static final int MAX_CHAR = 126;
    private static final int START_CHAR_WITH_ESCAPE = 32;
    private static final int START_CHAR_WITHOUT_ESCAPE = 97;

    public static String randomStringWithLength(final int length) {
        StringBuilder builder = new StringBuilder();
        Random r = new Random();
        for (int index = 0; index < length; index++) {
            builder.append(
                    (char) (r.nextInt(MAX_CHAR - START_CHAR_WITH_ESCAPE - 1)
                            + START_CHAR_WITH_ESCAPE));
        }
        return builder.toString();
    }

    public static String randomStringWithoutEscapeCharWithLength(final int length) {
        StringBuilder builder = new StringBuilder();
        Random r = new Random();
        for (int index = 0; index < length; index++) {
            builder.append(
                    (char) (r.nextInt(MAX_CHAR - START_CHAR_WITHOUT_ESCAPE - 1)
                            + START_CHAR_WITHOUT_ESCAPE));
        }
        return builder.toString();
    }

    private StringTestUtil() {
        throw new UnsupportedOperationException(
                "This is a utils class and cannot be instantiated");
    }
}
